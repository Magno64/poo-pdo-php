<?php 
	require_once 'Cart.php';
	require_once 'Product.php';

	class Store{
		//aquí esta mos llamando los traits en las clases se pueden crear direnta mente  los traits pero en esta caso se muestra que tambien se pueden llmar de otro archivos, tambien las clases  no deben de ser llamadas como un trait.
		use Cart, Product;
		public $fltTotal = 0;

		public function getCart(){
			$this->fltTotal = $this->fltPrice * $this->intQuantity;
			$infoCart = "<h2>Cart</h2>
				<hr>
				Product : {$this->strProduct} <br>
				Quantity : {$this->intQuantity} <br>
				Price : {$this->fltPrice} <br>
				Total : {$this->fltTotal} <br>";
			return $infoCart;
		}

		public function setStock(int $quantity){
			$this->intStock = $this->intStock - $quantity;
		}

	}// end class

 ?>