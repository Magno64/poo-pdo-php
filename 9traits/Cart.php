<?php 
	// los trait  son como clases que se pueden heredar pero no se pueden instanciar en objetos.
	trait Cart{
		public $strProduct; 
		public $intQuantity;

		public function setCart(string $product, int $quantity){

			$this->strProduct = $product;
			$this->intQuantity = $quantity;
		} 

		abstract public function getCart();

	}


 ?>