<?php 
	trait Product{
		public $strProduct;
		public $fltPrice;
		public $intStock;

		public function setProduct(string $product, float $price, int $stock){
		$this->strProduct = $product;
		$this->fltPrice = $price;
		$this->intStock = $stock;
		}

		public function getProduct():string{
			$strInfo = "
				Product: {$this->strProduct} <br>
				Price: {$this->fltPrice} <br>
				Stock: {$this->intStock} <br><br>";
			return $strInfo;
		}
	}







 ?>