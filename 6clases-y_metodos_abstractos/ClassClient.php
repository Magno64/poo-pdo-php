<?php 

	require_once ("ClassPerson.php");

	class Client extends Person{

		protected $fltCredit;

		function __construct(int $dpi, string $name, int $age){

				parent::__construct($dpi, $name, $age);
		}


		public function setCredit(float $credit){
			$this->fltCredit = $credit;
		}

		public function getCredit():float{

			
			return $this->fltCredit;
		}

		public function getPersonalInfo(){
			$info = "<h2>PERSONAL DATA</h2> DPI: {$this->intDpi}<br> Name: {$this->strName}<br> Age: {$this->intAge}<br>
			";

			return $info;
		}

		public function setInfo(string $info){
			$this->info = $info;
		}

		public function getInfo():string{
			return $this->info.' '.$this->strName;
		}


	}//End Class Client

 ?>