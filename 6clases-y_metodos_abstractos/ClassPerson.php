<?php 
	

	//las clases abtractas  deben cotener todos sus metodos abstractos vacios sin cuerpo como se ve acontinuación, estos metodos se pueden redefinir en la clases  que los heredan.como lo vamos a aver en la clases employee
	abstract class Person{

		public $intDpi;
		public $strName;
		public $intAge;

		function __construct (int $dpi, string $name, int $age){
			$this->intDpi = $dpi;
			$this->strName = $name;
			$this->intAge = $age;
		}

		abstract public function getPersonalInfo();
		abstract public function setInfo(string $info);
		abstract public function getInfo():string;
	}//end class Person


 ?>