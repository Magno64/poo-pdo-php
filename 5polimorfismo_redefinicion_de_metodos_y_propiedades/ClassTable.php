<?php 

	require_once ("ClassForniture.php");

	/**
	 * 
	 */
	class Table extends Forniture
	{	
		public $strShape = "";
		protected $strSize;

		public function __construct(string $description, float $price, string $color, string $material, string $size){
			parent::__construct($description, $price, $color, $material);
			$this->strSize = $size;
		}

		public function setShape(string $shape){
			$this->strShape = $shape;
		}

		public function setStatus(string $status){
			$this->strStatus =$status;
		}

		public function getProductInfo(){
			$arrayProduct = array('Product' => $this->strDescription, 
				'Price' => $this->fltPrice,
				'Stock_minimum' => $this->intStockMinimum,
				'Status' => $this->strStatus, 
				'Color' => $this->strColor,
				'Material' => $this->strMaterial,
				'Size' => $this->strSize,
				'Shape' => $this->strShape);
			return $arrayProduct;
		}
		
		
	}//end class table

 ?>