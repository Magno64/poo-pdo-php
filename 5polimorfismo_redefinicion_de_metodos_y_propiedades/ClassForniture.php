<?php 

	require_once ("ClassProduct.php");

	class Forniture extends Product{

		public $strColor;
		public $strMaterial;
		public $strStatus = "Out of stock";	

		public function __construct(string $description, float $price, string $color, string $material){
			parent::__construct($description, $price);
			$this->strColor = $color;
			$this->strMaterial = $material;
		}

		public function getProductInfo(){
			$arrayProduct = array('Product' => $this->strDescription,
				'Price' => $this->fltPrice,
				'Stock_minimum' => $this->intStockMinimum,
				'Status' => $this->strStatus,
				'Color' => $this->strColor,
				'Material' => $this->strMaterial);
			return $arrayProduct;
		}	


	}//End Class Forniture
	
 ?>