<?php 

	class Product{
		public $strDescription;
		public $fltPrice;
		protected $intStockMinimum = 10;
		protected $strStatus = "Active";

		public function __construct(string $description, float $price){
			$this->strDescription = $description;
			$this->fltPrice = $price;
		}

		public function getProductInfo(){
			$arrayProduct = array('Product' => $this->strDescription,
				'Price' => $this->fltPrice,
				'Stock_minimum' => $this->intStockMinimum,
				'Status' => $this->strStatus);
			return $arrayProduct;
		}
	}//End Class Product

 ?>