<?php 
	
	//require_once ("../controllers/Employee.php");

	//require_once ("../controllers/Client.php");
	//require_once '../controllers/Person.php';
	require_once '../autoload.php';

	use controllers\Employee;
	use controllers\Client;
	use controllers\Person;

	$objPerson = new Person();

	echo $objPerson->greet();
	//intanciando la clase en el objeto con propiedades y metodos heredados de una super clase
	$objEmployee = new Employee(279404,"Jorge Diaz", 26);

	//seteando datos aun metodo de la clase empleado 
	$objEmployee->setJobTitle("Admin");


	echo $objEmployee->getPersonalInfo();

	echo $objEmployee->getJobTitle();

	$objClient = new Client(942794,"Enrique Rivera", 26);
	$objClient->setCredit(1000);

	echo $objClient->getPersonalInfo();
	echo "Credit: ".$objClient->getCredit();




 ?>