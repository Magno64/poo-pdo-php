<?php 
	
	// se declara la funcion para que carge de manera automaticamente los archivos requeridos o heredados por distintas clases para no estar requirendo o clases por clase o archivo por archivo 
	function autoload($class){
		
		//echo $class."<br>";
		$url = "".str_replace("\\","/",$class.".php");
		//echo $url;
		require_once ($url);
	}

	//metedoto por el cual realiza la busca en el mismo directorio de los arichos requeridos 
	spl_autoload_register('autoload');


 ?>