<?php 
	namespace Models;
	class Person{

		public $intDpi;
		public $strName;
		public $intAge;

		function __construct (int $dpi, string $name, int $age){
			$this->intDpi = $dpi;
			$this->strName = $name;
			$this->intAge = $age;
		}

		public function getPersonalInfo(){
			$info = "<h2>PERSONAL DATA</h2> DPI: {$this->intDpi}<br> Name: {$this->strName}<br> Age: {$this->intAge}<br>
			";

			return $info;
		}
	}//end class Person


 ?>