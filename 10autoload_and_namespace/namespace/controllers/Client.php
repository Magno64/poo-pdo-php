<?php 
	namespace Controllers;
	//require_once ("../models/Person.php");
	//require_once '../autoload.php';

	// no es necesario requerir los archivos ya que se estan requiriendo en el archivo company
	use models\Person;

	class Client extends Person{

		protected $fltCredit;

		function __construct(int $dpi, string $name, int $age){

				parent::__construct($dpi, $name, $age);
		}


		public function setCredit(float $credit){
			$this->fltCredit = $credit;
		}

		public function getCredit():float{

			
			return $this->fltCredit;
		}


	}//End Class Client

 ?>