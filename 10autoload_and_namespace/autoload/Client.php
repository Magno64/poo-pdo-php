<?php 

	//require_once ("ClassPerson.php");
	require_once ('autoload.php');

	
	class Client extends Person{

		protected $fltCredit;

		function __construct(int $dpi, string $name, int $age){

				parent::__construct($dpi, $name, $age);
		}


		public function setCredit(float $credit){
			$this->fltCredit = $credit;
		}

		public function getCredit():float{

			
			return $this->fltCredit;
		}


	}//End Class Client

 ?>