<?php 
	
	class User{

		// public significa que podemos acceder a estas propiedades o métodos desde cualquier lado; private nos limita a que nosotros no podemos acceder desde cualquier lado ni siquiera desde una subclase es decir una clase que lo heredan; protected en este caso se puede acceder  a esta propiedad dese la misma clase donde  se crea como tambien en las subclases que la heredan; static quiere decir que tiene una valor inicial y ese valor no va a cambiar un ejemplo el pi

		private $strName;
		private $strMail;
		private $strType;
		private $strPass;
		protected $strRegistrationDate;
		static $strState = "Active"; 

		function __construct(string $name, string $mail, string $type){

			$this->strName = $name;
			$this->strMail = $mail;
			$this->strType = $type;
			$this->strPass = rand();
			$this->strRegistrationDate = date('Y-m-d H:m:s');

		}

		public function getName():string{
				return $this->strName;
		}

		public function getMail():string{
				return $this->strMail;
		}

		public function getType():string{
				return $this->strType;
		}

		public function getPass():string{
				return $this->strPass;
		}

		//método o funcion para mostrar toda la información 
		public function getProfile(){

			echo "Datos del usuario <br> Nombre: ".$this->strName."<br> Mail: ".$this->strMail."<br> Tipo: ".$this->strType."<br> Clave: ".$this->strPass."<br> Estado: ".self::$strState."<br> Fecha de registro: ".$this->strRegistrationDate;
		}

		public function setChangePass(string $pass){
			$this->strPass = $pass;
		}


	}//end class user


 ?>