<?php 
	
	require_once ("ClassUser.php");

	//Instanciando primer objeto

	$objUser1 = new User("Jorge Diaz", "jdr@gmail.com", "Admin");

	//Instanciando el objeto andrea
	$objAndrea = new User("Andrea Rincon", "andrea@gmail.com", "Cliente");

	//echo $objUser1->strName."<br>".$objUser1->strMail;

	//echo $objUser1->getName()."<br>".$objUser1->getMail()."<br>".$objUser1->getType()."<br>".$objUser1->getPass();

	//llamando una propiedad o variable estatica
	//echo User::$strState;

	// imprimiendo el metodo getProfile con el objeto User1

	echo $objUser1->getProfile()."<br><br><br>";

	//Cambiar de contraseña usando el método o funcion setChangePass

	$objAndrea->setChangePass("123456789");


	// Imprimir el metodo getProfile con el objeto andrea
	echo $objAndrea->getProfile();


	
	


 ?>