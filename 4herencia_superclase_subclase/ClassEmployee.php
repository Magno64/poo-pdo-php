<?php 
	
	//importando la clase persona
	require_once ("ClassPerson.php");

	//definiendo la clase empleado y denotando que hereda los metodos y propiedades de la clases persona
	class Employee extends Person{

		protected $strJobTitle;

		function __construct(int $dpi, string $name, int $age){
			//aqui estamos utilizando el constructor de su elemento padre osea el constructor de la clase persona
			parent::__construct($dpi, $name, $age);
		}

		public function setJobTitle(string $jobTitle){
			$this->strJobTitle = $jobTitle;
		}

		public function getJobTitle():string{

			$data = "Job Title: ".$this->strJobTitle;

			return $data;

		}


	}//end class Employee


 ?>