<?php 

	require_once ("ClassEmployee.php");

	require_once ("ClassClient.php");
	//intanciando la clase en el objeto con propiedades y metodos heredados de una super clase
	$objEmployee = new Employee(279404,"Jorge Diaz", 26);

	//seteando datos aun metodo de la clase empleado 
	$objEmployee->setJobTitle("Admin");


	echo $objEmployee->getPersonalInfo();

	echo $objEmployee->getJobTitle();

	$objClient = new Client(942794,"Enrique Rivera", 26);
	$objClient->setCredit(1000);

	echo $objClient->getPersonalInfo();
	echo "Credit: ".$objClient->getCredit();




 ?>