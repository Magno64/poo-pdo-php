<?php

	class Operation{

		public $quantity1 = 0;
		public $quantity2 = 0;
		public $result = 0;

		function __construct($intQuantity1, $intQuantity2){

			$this->quantity1 = $intQuantity1;
			$this->quantity2 =$intQuantity2;
		}

		public function getSum(){
			$this->result = $this->quantity1 + $this->quantity2;
			return $this->result;
		}

		public function getRest(){
			$this->result = $this->quantity1 - $this->quantity2;
			return $this->result;
		}

		public function getMult(){
			$this->result = $this->quantity1 * $this->quantity2;
			return $this->result;
		}

		public function getDiv(){
			$this->result = $this->quantity1 / $this->quantity2;
			return $this->result;
		}

			

	}//End class operationo

?>