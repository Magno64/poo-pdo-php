<?php
	//estamos importando el archivo o requiriendoloo
	require_once ("ClassOperation.php");

	//Primer Objeto
	$operation1 = new Operation(10,2);

	//Segundo objeto
	$operation2 = new Operation(100,3);

	//LLamando metodos para el primero objeto
	$totalSum = $operation1->getSum();

	echo "Total suma: ".$totalSum."<br>";

	$totalRest = $operation1->getRest();

	echo "Total resta: ".$totalRest."<br>";

	$totalMult = $operation1->getMult();

	echo "Total multiplicación: ".$totalMult."<br>";

	$totalDiv = $operation1->getDiv();

	echo "Total division: ".$totalDiv."<br><br>";


	//LLamando metodos para el segundo objeto
	$totalSum = $operation2->getSum();

	echo "Total suma: ".$totalSum."<br>";

	$totalRest = $operation2->getRest();

	echo "Total resta: ".$totalRest."<br>";

	$totalMult = $operation2->getMult();

	echo "Total multiplicación: ".$totalMult."<br>";

	$totalDiv = $operation2->getDiv();

	echo "Total division: ".$totalDiv."<br><br>";




?>