<?php 

	interface Operation{

		public function squareRoot(float $num):float;
		public function power(int $num, int $power):int;

	}

 ?>