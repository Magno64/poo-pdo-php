<?php 

	require_once 'operation.php';
	require_once 'basicOperation.php';

	class Calculate Implements Operation,basicOperation
	{
		public function squareRoot(float $num):float
		{
			$total = sqrt($num);
			return $total;
		}

		public function power(int $num, int $power):int
		{
			$total = pow($num, $power);
			return $total;
		}

		public function basicOperation(float $num1, float $num2, string $operation){
			if($operation == "+"){
				$result = $num1 + $num2;
			}else if ($operation == "-") {
				$result = $num1 - $num2;
			}else if ($operation == "*") {
				$result = $num1 * $num2;
			}else if ($operation == "/") {
				$result = $num1 / $num2;
			}else{
				$result = "invalid operation";
			}

			return $result;
		}
		
		
	}
 ?>