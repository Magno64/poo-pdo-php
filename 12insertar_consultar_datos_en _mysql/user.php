<?php 
	/**
	 * 
	 */
	require_once 'autoload.php';

	class User extends Connection
	{
		private $strName;
		private $intTelefone;
		private $strEmail;

		function __construct()
		{
			$this->connection = new Connection();
			$this->connection = $this->connection->connect();
		}

		public function insertUser(string $name, int $telefone, string $email){

			$this->strName = $name;
			$this->intTelefone = $telefone;
			$this->strEmail = $email;
			// se raliza el conmando para setar datos en la tabla usuario de la base de datos
			$sql = "INSERT INTO user(name,telefone,email) VALUES(?,?,?)";
			// aquí atra vez cdel metodo prepare() se perpara para ejecutar la sentencias 
			$insert = $this->connection->prepare($sql);
			// se instancian en la variable  los datos que se van a insertar en la tabla user
			$arrData = array($this->strName, $this->intTelefone, $this->strEmail);
			// se ejecuta la insersuion de datos atra vez del metodo execute de php 
			$resInsert = $insert->execute($arrData);
			// se muestra el id de la ultima insercion hecha a la tabla a tra vez del metodo lastinsertid() de php
			$idInsert = $this->connection->lastinsertid();
			// se retorna el id de la ultima inserion
			return $idInsert;


		}

		public function getConnection(){
			return $this->connection;
		}


		public function getUsers(){
			// se declara la consulta en la tabla usario 
			$sql = "SELECT * FROM user";
			// se ejecuta el query  mediante el metodo query() de php 
			$execute = $this->connection->query($sql);
			// se almacenan en la variable request en un arreglo todos los datos insertados en la tabla me diante el metodo fetchall
			$request = $execute->fetchall(PDO::FETCH_ASSOC);
			return $request;

		}
	}

 ?>