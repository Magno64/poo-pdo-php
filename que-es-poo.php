<?php
	phpinfo()
//comentarios 
/* comentarios multilinea */

/*Un objeto es un elemnto fundamental en programacion que tiene propiedades y metodos */

//Ejemplo de Objeto 

//Balon

/*Propiedades 
	Tamaño
	Color
	Material
*/

/*Métdos
	 rodar()
	 rebotar()
*/

//Vehiculo

/*Propiedades
	Color
	Marca
	Modelo
	Matricula
*/

/*Métodos
	arrancar()
	acelerar()
	frenar()
*/

//Usuario

/*Propiedades
	Nombre
	Edad
	Género
	Telefono
*/

/*Métodos
	registrarse()
	login()
	comprar()
*/


/*Que es la clase 

es una plantilla para crear 	objetos donde vamos a definir y estructurar sus propiedas como tambien sus métodos. 

el proceso de crear un objeto en una clase se le llama  instancia
*/

/*Conceptos de la programación

	Abstracción:
		Se hace en base lo que va a hacer el objeto sus funcionalidades o métodos igualq ue sus propiedades
	Herencia
		Permite crear clases en clases ya existentes 

		ejemplo 
		Super class Persona
			Propiedades:
			Identificacion 
			Nombre
			Apellido
			Edad

		Sub class Empleado
			Propiedades:
			Puesto
			Sueldo
			Tipo de contrato

		Sub class Cliente
			Código
			Categoría
		
		Entonces las sub clases van a heredar tanto las propiedades como los métodos de la súper clase


	Modularización

		La modularización sirve para que el sistema este dividido en distintas partes un ejemplo modulo de cliente y modulo producto, entonces si llega a fallar el método de registrar producto en el modulo producto no afecta el modulo de cliente que es el que muestra los productos.

		De estamanera es que tenemos que estructurar las clases para llevar a cabo la modularización. 


	Encapsulamiento

		Es procedimiento de  protección y ocultamiento de las propiedades y métodos ineditas de un objeto 

	Polimorfismo

		Es la capacidad que tiene los objetos de una clase de responder a un evento y a un mensaje 
		ejemplo la super clases persona tiene el método registrar, y hay dos sub clases empleado, cliente las dos tienen el metodo heredado registrar ambas pueden hacer uso de este pero mostraran distintos mensajes funcionalidades o vistas. 

		


*/

?>


