<?php 
	
	class User{

		// public significa que podemos acceder a estas propiedades o métodos desde cualquier lado; private nos limita a que nosotros no podemos acceder desde cualquier lado ni siquiera desde una subclase es decir una clase que lo heredan; protected en este caso se puede acceder  a esta propiedad dese la misma clase donde  se crea como tambien en las subclases que la heredan; static quiere decir que tiene una valor inicial y ese valor no va a cambiar un ejemplo el pi 
		public $strName;
		public $strMail;
		public $strType;
		private $strPass;
		protected $strRegistrationDate;
		static $strState = "Active"; 

		function __construct(string $name, string $mail, string $type){

			$this->strName = $name;
			$this->strMail = $mail;
			$this->strType = $type;
			$this->strPass = rand();

		}




	}//end class user


 ?>